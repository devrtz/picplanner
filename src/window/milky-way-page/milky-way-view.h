/*
 * milky-way-view.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtk/gtk.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"
#include "calculations/calculations_milky_way.h"
#include "calculations/calculations_transformations.h"

G_BEGIN_DECLS

#define PICPLANNER_MILKYWAY_TYPE (picplanner_milkyway_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerMilkyway, picplanner_milkyway, PICPLANNER, MILKYWAY, GtkBox)


void
picplanner_milky_way_set_date (PicplannerMilkyway *milky_way,
                               GDateTime          *date_time);


void
picplanner_milky_way_set_rise_upper_set (PicplannerMilkyway *milky_way,
                                         GDateTime          *date_time,
                                         double             *coordinates_array,
                                         int                *index_rise_upper_set);

void
picplanner_milky_way_set_disturbance (PicplannerMilkyway   *milky_way,
                                      GDateTime		   *date_time_noon,
				      double		   *coordinates_array_sun,
				      double	           *coordinates_array_moon,
				      double	   	   *coordinates_array_milky_way);


PicplannerMilkyway *picplanner_milkyway_new ();

G_END_DECLS
