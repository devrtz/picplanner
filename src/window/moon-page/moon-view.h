/*
 * moon-view.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtk/gtk.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"
#include "calculations/calculations_transformations.h"

G_BEGIN_DECLS

#define PICPLANNER_MOON_TYPE (picplanner_moon_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerMoon, picplanner_moon, PICPLANNER, MOON, GtkBox)


void
picplanner_moon_set_date (PicplannerMoon  *moon,
                          GDateTime       *date_time);


void
picplanner_moon_set_rise_upper_set (PicplannerMoon  *moon,
                                    GDateTime       *date_time,
                                    double          *coordinates_array,
                                    int             *index_rise_upper_set);

void
picplanner_moon_set_illumination_intensity (PicplannerMoon  *moon,
                                            double          illumination,
                                            char            *phase);


PicplannerMoon *picplanner_moon_new ();

G_END_DECLS
