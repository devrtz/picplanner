/*
 * sun-view.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtk/gtk.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"
#include "calculations/calculations_transformations.h"

G_BEGIN_DECLS

#define PICPLANNER_SUN_TYPE (picplanner_sun_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerSun, picplanner_sun, PICPLANNER, SUN, GtkBox)


/*
 * Set the date which is showen at the top of the sun view.
 */
void
picplanner_sun_set_date (PicplannerSun  *sun,
                         GDateTime      *date_time);


/*
 * Set the index of the sunrise, upper culmination, sunset and lowest culmination
 * to present the information in the sun view.
 */
void
picplanner_sun_set_rise_upper_set (PicplannerSun  *sun,
                                   GDateTime      *date_time,
                                   double         *coordinates_array,
                                   int            *index_rise_upper_set_lower);


/*
 * Set the index of the dark night, blue hour and golden hour
 * to present the information in the sun view.
 */
void
picplanner_sun_set_dark_blue_golden (PicplannerSun  *sun,
                                     GDateTime      *date_time,
                                     int            *index_dark_blue_golden);


PicplannerSun *picplanner_sun_new ();

G_END_DECLS
