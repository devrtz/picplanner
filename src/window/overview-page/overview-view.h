/*
 * overview-view.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtk/gtk.h>
#include <shumate/shumate.h>

G_BEGIN_DECLS

#define PICPLANNER_OVERVIEW_TYPE (picplanner_overview_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerOverview, picplanner_overview, PICPLANNER, OVERVIEW, GtkBox)

PicplannerOverview
*picplanner_overview_new ();

void
picplanner_update_date_time (PicplannerOverview *overview);

GDateTime
*picplanner_overview_get_date_time (PicplannerOverview *overview);

void
picplanner_overview_update_date_time (PicplannerOverview *overview);

void
picplanner_overview_set_current_coordinates_sun (PicplannerOverview *overview,
                                                 double 						*array_coordinates_sun,
                                                 int 								*rise_upper_set_index,
                                                 int 								current_index);

void
picplanner_overview_set_current_coordinates_moon (PicplannerOverview 	*overview,
                                                  double 							*array_coordinates_moon,
                                                  int 								*rise_upper_set_index,
                                                  int 								current_index);

void
picplanner_overview_set_current_coordinates_milky_way (PicplannerOverview *overview,
                                                       double 						*array_coordinates_milky_way,
                                                       int 								*rise_upper_set_index,
                                                       int 								current_index);

void
picplanner_overview_remove_map_content (PicplannerOverview *overview);

void
picplanner_overview_update_map_center (PicplannerOverview *overview);

void
picplanner_overview_update_map_sun (PicplannerOverview  *overview,
                                    double              *array_coordinates_sun,
                                    int                 current_index,
                                    int                 *rise_upper_set_sun);

void
picplanner_overview_update_map_moon (PicplannerOverview  *overview,
                                     double              *array_coordinates_moon,
                                     int                 current_index,
                                     int                 *rise_upper_set_moon,
                                     int                 phase_moon_index);

void
picplanner_overview_update_map_milky_way (PicplannerOverview  *overview,
                                          double              *array_coordinates_milky_way,
                                          int                 current_index,
                                          int                 *rise_upper_set_milky_way);

G_END_DECLS
